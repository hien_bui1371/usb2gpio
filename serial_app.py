import serial
import math
import time
import threading
import logging

logger = logging.getLogger()

class LUC:



    def __init__(self, com):

        self.ser = serial.Serial(com, timeout=0.1, baudrate=57600)
        if (self.ser == None):
            raise NameError('SerialPortNotPresent') 
        self.ser.reset_output_buffer()
        self.ser.reset_input_buffer()
        
        self.__uart_buffer = ''
        self._stop_event = threading.Event()


    def flushData(self, data):
        self.ser.write(data)
        logger.info(data)
    
    def printData(self):
        lines = self.ser.readlines()
        for line in lines:
            print(line.decode("utf-8")) 

# a          - get all I/O values
# <nn>i      - get input <nn>
# <on>,<nn>o - set output <nn> <on> (nn=pinNr 1..16, <on>=0-off/1-on)
# <nn>n      - set node ID (0..7)
# <n>l       - set activity led on/off (0: off, 1: on)
# <n>d       - switch on pulldown inputs (0: INPUT, 1: INPUT_PULLDOWN)
# <n>u       - switch on pullup inputs (0: INPUT, 1: INPUT_PULLUP)
# v          - display board name and board id
# h          - this help

    def print_board_info(self):
        self.flushData(b'v')
        self.printData()

    def print_help(self):
        self.flushData(b'h')
        self.printData()

    def get_all(self):
        self.flushData(b'a')
        self.printData()

    def get_input(self, pinIndex):
        data = str(pinIndex).zfill(2) + "i"
        self.flushData(data.encode())
        self.printData()

    def set_output(self, status, pinIndex):
        data = str(status) + ","+ str(pinIndex).zfill(2) + "o"
        self.flushData(data.encode())
        self.printData()

    def set_node(self, nodeId):
        data = str(nodeId) + "n"
        self.flushData(data.encode())
        self.printData()

    def set_led(self, status):
        data = str(status) + "l"
        self.flushData(data.encode())
        self.printData()

    def pull_up(self, pinIndex):
        data = str(pinIndex).zfill(2) + "u"
        self.flushData(data.encode())
        self.printData()

    def pull_down(self, pinIndex):
        data = str(pinIndex).zfill(2) + "d"
        self.flushData(data.encode())
        self.printData()


    def __isStartingChar(self,c):
        return ((c == 't') or (c == 'r'))

    def __rxThread(self):
        while True:
            if (self._stop_event.is_set()):
                break
                
            while(self.ser.in_waiting > 0):                
                cc = self.ser.read(self.ser.in_waiting).decode('ascii')
                self.__uart_buffer += (cc)                                   
            time.sleep(0.001)
    



    def deInitSerial(self):
        self.ser.flush() 
        self.ser.close()
        del self.ser

    def __del__(self):
        self.deInitSerial()
        