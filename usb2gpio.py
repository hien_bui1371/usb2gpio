import sys
import serial
import serial.tools.list_ports
ports = serial.tools.list_ports.comports()
from serial_app import LUC

for port, desc, hwid in sorted(ports):
    if 'USB VID:PID=1EAF:0004' in hwid:
        print("Detected 'Usb2Gpio' device at port {}".format(port))
        break
else:
    print("Cannot detectect 'Usb2Gpio' device")
    sys.exit()

connection = LUC(port)
from time import sleep
# connection.pull_down(5)
# connection.pull_up(5)
# connection.set_led(1)
for i in range(0,5):
    connection.set_output(1, 23)
    connection.set_led(1)
    sleep(1)
    connection.set_output(0, 23)
    connection.set_led(0)
    sleep(1)
# connection.get_all()
# connection.get_input(5)
# connection.print_board_info()
# connection.print_help()

